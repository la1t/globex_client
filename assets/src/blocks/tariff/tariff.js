require('./tariff.scss');
const $ = require('jquery');

$('.tariff__desc-open-btn').on('click', function() {
    $(this).toggleClass('tariff__desc-open-btn_active')
        .siblings('.tariff__description')
        .toggleClass('tariff__description_active');
});
'use strict';

require('./table.scss');
require('./paypal-logo-129x32.png');
require('./sberbank.png');
require('./article-img-stub.png');
const $ = require('jquery');

$('.table__popup-toggler').on('click', function() {
    $(this).toggleClass('btn_active')
        .parents('.table__row')
        .toggleClass('table__row_focused')
        .find('.table__popup')
        .toggleClass('table__popup_active');
});
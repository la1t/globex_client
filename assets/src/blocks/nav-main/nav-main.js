'use strict';

require('./nav-main.scss');
const $ = require('jquery');

$('.nav-main__burger').on('click', function() {
    $(this).parents('.nav-main').toggleClass('nav-main_inactive');
});

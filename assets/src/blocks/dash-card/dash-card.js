require('./dash-card.scss');
require('./dash-num-users.png');
require('./dash-file.png');
require('./dash-information.png');
require('./dash-presentation.png');
require('./dash-refferal.png');
require('./dash-favorite-articles.png');
require('./dash-finance.png');
require('./dash-saved-articles.png');
const $ = require('jquery');



function resizeCards() {
    let maxHeight = 0;
    $('.dash-card').each(function() {
        $(this).attr('style', '');
    }).each(function() {
        if ($(this).height() > maxHeight) {
            maxHeight = $(this).height();
        }
    }).each(function() {
        $(this).height(maxHeight);
    });
}

resizeCards();

$(window).on('resize', resizeCards);
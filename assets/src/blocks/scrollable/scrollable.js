'use strict';

require('./scrollable.scss');
const $ = require('jquery');
require('malihu-custom-scrollbar-plugin')($);
require('malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css');

$('.scrollable').mCustomScrollbar({
    axis: 'y',
    scrollInertia: 250,
    theme: 'custom',
});